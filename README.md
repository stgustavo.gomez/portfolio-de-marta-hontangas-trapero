# Portfolio de Marta Hontangas Trapero

Web que muestra los proyectos de Marta (diseñadora). Se trata de un proyecto orientado a ser visualizado de forma horizontal con un menú inicial que indexa todas las secciones del mismo. Empleo GreenSock, ScrollMagic y  JQuery entre otras librerías.

http://www.martahontangastrapero.com/
