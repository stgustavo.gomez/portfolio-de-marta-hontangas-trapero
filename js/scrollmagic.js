/////////////// SCROLLMAGIC DE 'SOBRE MI' ///////////////


	var controller = new ScrollMagic.Controller({vertical: false});


	var scene = new ScrollMagic.Scene({
							triggerElement: "#secPresentation",
							triggerHook: .7
						})
						.setTween(TweenMax.to("#secVision .scrollIndic", .5, {opacity: 0, ease: Linear.easeNone})) // trigger a TweenMax.to tween
						.addTo(controller);


	var scene = new ScrollMagic.Scene({
							triggerElement: "#secPresentation .secContent",
							triggerHook: .7
						})
						.setTween(TweenMax.to("#secPresentation .secContent .animScroll", .5, {opacity:1, left: 0, ease: Linear.easeNone})) // trigger a TweenMax.to tween
						.addTo(controller);

	var scene = new ScrollMagic.Scene({
							triggerElement: "#secWhy .secContent",
							triggerHook: .7
						})
						.setTween(TweenMax.to("#secWhy .secContent .animScroll", .5, {opacity:1, left: 0, ease: Linear.easeNone})) // trigger a TweenMax.to tween
						.addTo(controller);


	var scene = new ScrollMagic.Scene({
							triggerElement: "#secExperiencia .secContent",
							triggerHook: .9
						})
						.setTween(TweenMax.to("#secExperiencia .secContent .animScroll", .5, {opacity:1, left: 0, ease: Linear.easeNone})) // trigger a TweenMax.to tween
						// .addIndicators()
						.addTo(controller);


/////////////// !SCROLLMAGIC DE 'SOBRE MI' ///////////////




/////////////// SCROLLMAGIC DE 'PROYECTOS' ///////////////

	// $(".project .images .imgScroll").each(function() {

	//   	var scene = new ScrollMagic.Scene({
	// 						triggerElement: $(this).parent()[0],
	// 						duration: 300,
	// 						triggerHook: .8
	// 					})
	// 					.setTween(TweenMax.fromTo(this, .2, {scale: .3}, {scale: 1})) // trigger a TweenMax.to tween
	// 					// .addIndicators()
	// 					.addTo(controller);

	// });

	// $(".project .images .img, .project .images .vid").each(function() {


	// 	var scene = new ScrollMagic.Scene({
	// 						triggerElement: $(this).parent()[0],
	// 						duration: 200,
	// 						triggerHook: 0,
	// 						offset: 200
	// 					})
	// 					.setTween(TweenMax.to(this, .2, {scale: .3})) // trigger a TweenMax.to tween
	// 					// .addIndicators()
	// 					.addTo(controller);
	// });

	


/////////////// !SCROLLMAGIC DE 'PROYECTOS' ///////////////


