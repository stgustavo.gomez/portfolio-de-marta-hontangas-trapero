
var menuOpen = true;
var contactOpen, aboutOpen, projectOpen = false;

// var homeLeft = true;

//VARIABLE QUE SEÑALA SI SE ESTÁ HACIENDO UNA ANIMACIÓN DE CAMBIO DE PANTALLA
var onAnim = false;

//SECCION EN LA QUE NOS ENCONTRAMOS
var project = 'Microteatro';

$(document).ready(function() {

	console.log(getOS());

	$('.logoAsterisco').click(function(){
		if(!menuOpen){
			openMenu();
			TweenMax.to($('#persInfo .selected .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear, onComplete: function(){
				$('#persInfo .selected').removeClass('selected');
			}});
		}
	});

	// FUNCION PARA HOVER/ABRIR SOBRE MI

	$('#persInfo #about').hover(
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #about .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #about .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});

	$('#persInfo #about').click(function(){


		$('#persInfo #about').addClass('selected');

		$('#persInfo #contact, #persInfo #projects').removeClass('selected');

		TweenMax.to($('#persInfo #about .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});

		TweenMax.to($('#persInfo #contact .underline, #persInfo #projects .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});

		if(menuOpen){
			closeMenu();
			$('#homeWrap').css({'display': 'grid', 'opacity': '1'});

			$('#secPresentation, #secExperiencia, #secDownload').css('display', 'block');

			var delay = 1.3;
			TweenMax.to($(this).find('.selected'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
			TweenMax.to("#secVision .indexText", .5, {opacity:1, ease: Linear.easeNone, delay: delay-=.3});
			TweenMax.to("#secVision .secContent.definition", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});

			aboutOpen = true;
		}
		if(contactOpen){
			var delay = 0;
			$('#secPresentation, #secExperiencia, #secDownload').css({'display': 'block', 'opacity' : '0'});
			
			TweenMax.to($(this).find('.selected'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
			TweenMax.to($(this).siblings().find('.selected'), 0.2,{opacity: 0, ease:Power0.linear, delay: delay});
			TweenMax.to("#secVision .secContent.contact", .5, {opacity:0, ease: Linear.easeNone, delay: delay+=.3});
			TweenMax.to("#secVision .secContent.definition", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});

			setTimeout(function(){
					aboutOpen = true;
					contactOpen = false;
					$('#secPresentation, #secExperiencia, #secDownload').css('opacity' , '1');
				}, 1000);
		}
		if(projectOpen){
			var delay = 1.3;

			TweenMax.to($('.project .mainInfo, .project .images, .project .content'), .5,{opacity: 0, ease:Power0.linear, delay: 0, onComplete: function(){

					setTimeout(function(){
						$('#projectWrap, #projectWrap #' + project).css('display', 'none');
					},100);
					
					$('#homeWrap').css('display', 'grid');

					$('#secPresentation, #secExperiencia, #secDownload').css('display', 'block');

					$('.project .images').css({'padding-left': '50vw', 'opacity': '0'});
					$('.project .content').css({'margin-left': '3vw', 'opacity': '0'});
					
			}});

			TweenMax.to($('#homeWrap'), .5,{opacity: 1, ease:Power0.linear, delay: 1});

			TweenMax.to($(this).find('.selected'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
			TweenMax.to("#secVision .indexText", .5, {opacity:1, ease: Linear.easeNone, delay: delay-=.3});
			TweenMax.to("#secVision .secContent.definition", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});

			aboutOpen = true;
			projectOpen = false;

		}
	});

	//! FUNCION PARA ABRIR SOBRE MI


	// FUNCION PARA HOVER/ABRIR PROYECTOS

	$('#persInfo #projects').hover(
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #projects .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #projects .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});

	$('#persInfo #projects').click(function(){

		if(!menuOpen){
			openMenu();
			TweenMax.to($('#persInfo .selected .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear, onComplete: function(){
				$('#persInfo .selected').removeClass('selected');
			}});
		}
		else{

			if(!projectOpen){

				$('#persInfo #projects').addClass('selected');

				$('#persInfo #contact, #persInfo #about').removeClass('selected');

				TweenMax.to($('#persInfo #projects .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});

				TweenMax.to($('#persInfo #contact .underline, #persInfo #about .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});

				if(menuOpen){
					$('#projectWrap').css('display', 'grid');
					closeMenu();
					var delay = 1.3;
					projectOpen = true;
					project = 'Microteatro';
					TweenMax.to($('#' + project + ' .mainInfo'), .5,{opacity: 1, ease:Power0.linear, delay: delay, onStart: function () {
						$('#' + project).css('display', 'flex');
							openEjecution();
						}});
				}
				
			}

		
		}

	});

	// !FUNCION PARA HOVER/ABRIR PROYECTOS


	// FUNCION PARA HOVER/ABRIR CONTACTO

	$('#persInfo #contact').hover(
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #contact .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('#persInfo #contact .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});

	$('#persInfo #contact').click(function(){

		$('#persInfo #contact').addClass('selected');

		$('#persInfo #about, #persInfo #projects').removeClass('selected');

		TweenMax.to($('#persInfo #contact .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});

		TweenMax.to($('#persInfo #about .underline, #persInfo #projects .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});


		if(menuOpen){
			closeMenu();
			$('#homeWrap').css({'display': 'grid', 'opacity': '1'});

			$('#secPresentation, #secExperiencia, #secDownload').css('display', 'none');

			var delay = 1.3;
			TweenMax.to($(this).find('.selected'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
			TweenMax.to("#secVision .indexText", .5, {opacity:1, ease: Linear.easeNone, delay: delay-=.3});
			TweenMax.to("#secVision .secContent.contact", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});

			contactOpen = true;
		}
		
		if(projectOpen){
			var delay = 1.3;

			$('#secPresentation, #secExperiencia, #secDownload').css('display', 'none');

			TweenMax.to($('.project .mainInfo, .project .images, .project .content'), .5,{opacity: 0, ease:Power0.linear, delay: 0, onComplete: function(){

					setTimeout(function(){
						$('#projectWrap, #projectWrap #' + project).css('display', 'none');
					},100);

					$('.project .images').css({'padding-left': '50vw', 'opacity': '0'});
					$('.project .content').css({'margin-left': '3vw', 'opacity': '0'});
					
					$('#homeWrap').css('display', 'grid');
					
			}});

			TweenMax.to($('#homeWrap'), .5,{opacity: 1, ease:Power0.linear, delay: 1});

			TweenMax.to($(this).find('.selected'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
			TweenMax.to("#secVision .indexText", .5, {opacity:1, ease: Linear.easeNone, delay: delay-=.3});
			TweenMax.to("#secVision .secContent.contact", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});



			contactOpen = true;
			projectOpen = false;

		}
	});



	// FUNCION PARA ABRIR CONTACTO

	// FUNCION PARA ABRIR LAS SECCIONES

	$('#menuSectionsCont .menuSec').click(function(){

		if($(this).attr('id') == 'menuSec1' || $(this).attr('id') == 'menuSec2'){
		
			$('#projectWrap').css('display', 'grid');
			closeMenu();
			var delay = 1.3;
			projectOpen = true;

			switch($(this).attr('id')){
				case 'menuSec1':
					project = 'Microteatro';
					TweenMax.to($('#' + project + ' .mainInfo'), .5,{opacity: 1, ease:Power0.linear, delay: delay, onStart: function () {
						$('#' + project).css('display', 'flex');
						$('.vidMicro1')[0].play();
						$('.vidMicro2')[0].play();
						$('.vidMicro3')[0].play();
						$('.vidMicro4')[0].play();
						$('.vidMicro5')[0].play();
						openEjecution();
					}});
				break;

				case 'menuSec2':
					project = 'Filmaffinity';
					TweenMax.to($('#' + project + ' .mainInfo'), .5,{opacity: 1, ease:Power0.linear, delay: delay, onStart: function () {
						$('#' + project).css('display', 'flex');
						$('.vidFilma1')[0].play();
						openEjecution();
					}});
				break;

				// case 'menuSec3':
				// 	project = 'petAPorter';
				// 	TweenMax.to($('#' + project + ' .mainInfo'), .5,{opacity: 1, ease:Power0.linear, delay: delay, onStart: function () {
				// 		$('#' + project).css('display', 'flex');
				// 		openEjecution();
				// 	}});
				// break;
			}
		}
	});


	// FUNCION QUE NOS DEVUELVE TRUE CUANDO ALCANCEMOS LA IZQUIERDA DE HOME

	 // $("#homeWrap").scroll(function(){

  //       var div = $(this);
        
  //       if(div.scrollLeft() == 0)
  //       {
  //           homeLeft = true;
  //       }
  //       else{
  //       	homeLeft = false;
  //       }
  //   });

	 // !FUNCION QUE NOS DEVUELVE CUANDO ALCANCEMOS LA IZQUIERDA DE HOME

	 // FUNCIONES DEL MENÚ ABIERTO


	$('#menuSectionsCont .menuSec').hover(
		function(){
			TweenMax.to($(this).find('.txtSecCont'), 0.2,{color: '#303030', ease:Power0.linear});
			TweenMax.to($(this).find('.imgSecCont'), 0.2,{css:{filter: 'grayscale(0%);'}, ease:Power0.linear});
		},
		function(){
			TweenMax.to($(this).find('.txtSecCont'), 0.2,{color: '#000000', ease:Power0.linear});
			TweenMax.to($(this).find('.imgSecCont'), 0.2,{css:{filter: 'grayscale(100%);'}, ease:Power0.linear});
		}
	);

	// !FUNCIONES DEL MENÚ ABIERTO


	// FUNCIONES DE EXPERIENCIAS DE HOME


	$('#experiencesCont .experience').click(
		function(){
			if(!$(this).hasClass("active")){
				$(this).addClass("active");
				$(this).siblings().removeClass("active");

				$(this).css('border-top', '1px solid hsla(0, 0%, 0%, 1)');
				$(this).siblings().removeAttr('style');

				$(this).find('.cross.link').css('pointer-events', 'none');
				$(this).siblings().find('.cross.link').css('pointer-events', 'all');

				TweenMax.to($(this).find('.crossStatic'), 0.2,{opacity: 0, ease:Power0.linear, onComplete: function(){

				}});
				TweenMax.to($(this).siblings().find('.crossStatic'), 0.2,{opacity: 1, ease:Power0.linear, onComplete: function(){
						$(this).siblings().find('.crossStatic').fadeIn();
				}});
				
				var project = $(this).attr('id').split('exp')[1];
				TweenMax.to($('#descriptionsCont #desc' + project), 0.4,{opacity: 1, marginLeft: 0, ease:Power0.linear, onComplete: function(){
						$('#descriptionsCont #desc' + project).css('z-index', '2');
				}});
				TweenMax.to($('#descriptionsCont #desc' + project).siblings(), 0.2,{opacity: 0, marginLeft: '2vw', ease:Power0.linear, onComplete: function(){
						$('#descriptionsCont #desc' + project).siblings().css({'margin-left' : '-2vw', 'z-index' : '1'});
				}});

			}
		}
	);

	// !FUNCIONES DE EXPERIENCIAS DE HOME


	// FUNCION HOVER/IR A EJECUCIÓN EN PROYECTOS

	$('.ejec').click(function(){
		openEjecution();
	});

	$('.ejec').hover(
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('.ejec .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('.ejec .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});

	// !FUNCION IR A EJECUCIÓN EN PROYECTOS

	// FUNCION HOVER/IR A FICHA TECNICA EN PROYECTOS

	$('.tec').click(function(){
		openTecnica();
	});

	$('.tec').hover(
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('.tec .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
			if(!$(this).hasClass('selected'))
				TweenMax.to($('.tec .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});

	// !FUNCION IR A FICHA TECNICA EN PROYECTOS


	// FUNCION DE VOLVER A LA IZQUIERA DE LA HOME

	// $('#secDownload .goBack').click(function(){

	// 	TweenMax.to($('#homeWrap'), .4,{x: 0, ease:Power0.linear});

	// });

	$('#secDownload .goBack').hover(
		function(){
				TweenMax.to($('#secDownload .goBack .underline'), .4,{scaleX: 1, transformOrigin:"0", ease:Power0.linear});
		},
		function(){
				TweenMax.to($('#secDownload .goBack .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});
		});


	// !FUNCION DE VOLVER A LA IZQUIERA DE LA HOME


	// FUNCION DE SCROLL HORIZONTAL WHILE VERTICAL

	// $("#homeWrap, #projectWrap").mousewheel(function(event, delta) {
	// 	if(getOS() == 'Windows'){
	// 		this.scrollLeft -= (delta * 50);

	// 		// $(this).animate({
	// 		//  	scrollLeft: this.scrollLeft -= (delta * 25)
	// 		//  }, 15);

	// 	}
	// 	else{
	// 		this.scrollLeft = 0;

	// 	}

	// });

	

	// !FUNCION DE SCROLL HORIZONTAL WHILE VERTICAL



});

// PROJECT FUNCTIONS

	function openEjecution() {

		// TweenMax.to($('.project .images'), .6,{'margin-left': '0', ease:Power0.linear})

		TweenMax.to($('.tec .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});

		TweenMax.to($('.ejec .underline'), .4,{scaleX: 1,  transformOrigin:"0", ease:Power0.linear, onStart: function(){
			$('.ejec').addClass('selected');
			$('.tec').removeClass('selected');
		}});

		TweenMax.to($('.project .content'), 0.4,{opacity: 0, ease:Power0.linear, 
			onComplete: function(){
				$("#" + project + " .content").css({'display': 'none', 'margin-left': '3vw'});
			}
		});
		TweenMax.to($('.project .images'), 0.4,{paddingLeft: '10vw', ease:Power0.linear, 
			onStart: function(){
				$("#" + project + " .images").css({'display': 'flex', 'opacity' : 1});
			},
			delay : .4
		});

		TweenMax.to($('.project .images .img1'), .2, {scale: 1, delay:.4, onComplete: function(){
				$('.project .images .img1').css('transform-origin', 'bottom');
		}});

	}

	function openTecnica() {

		TweenMax.to($('.ejec .underline'), .4,{scaleX: 0, transformOrigin:"100%", ease:Power0.linear});

		TweenMax.to($('.tec .underline'), .4,{scaleX: 1,  transformOrigin:"0", ease:Power0.linear, onStart: function(){
			$('.tec').addClass('selected');
			$('.ejec').removeClass('selected');
		}});
		
		TweenMax.to($('.project .images'), 0.4,{opacity: 0, ease:Power0.linear, 
			onComplete: function(){
				$("#" + project + " .images").css({'display': 'none', 'padding-left': '50vw'});
			}
		});
		TweenMax.to($('.project .content'), 0.4,{opacity: 1, marginLeft: 0, ease:Power0.linear, 
			onStart: function(){
				$("#" + project + " .content").css({'display': 'flex'});
			},
			delay : .4
		});
	}


// !PROJECT FUNCTIONS


// MENU FUNCTIONS

function openMenu() {

	if(!onAnim){

		onAnim = true;

		var delay = 0;

		TweenMax.to("#header #headerBg", .3, {opacity: 0, ease: Linear.easeNone, delay: delay});
		// TweenMax.to("#menuHamCont", .5, {css:{borderBottomColor:"hsla(0,0%,0%,.19)", borderRightColor:"hsla(0,0%,0%,.19)"}, ease: Ease.none, delay: delay});
		// TweenMax.to("#menuHam", .5, {rotation: 90, ease: Ease.none, delay: delay});
		TweenMax.to("#menuHam > .line", .5, {css:{backgroundColor: '#000000'}, ease: Ease.none, delay: delay});
		// TweenMax.to("#menuHam > .line1", .5, {rotation: -45, top: '1.3vh', ease: Ease.none, delay: delay});
		// TweenMax.to("#menuHam > .line2", .5, {rotation: 45, top: '1.3vh', ease: Ease.none, delay: delay});
		TweenMax.to("#ems .em", .5, {css:{color: '#000000'}, ease: Ease.none, delay: delay});
		TweenMax.to("#persInfo > div", .5, {css:{color: '#000000'}, ease: Ease.none, delay: delay});

		TweenMax.to(".iconCont .logoB", .5, {css:{opacity: 0}, ease: Ease.none, delay: delay});
		TweenMax.to(".iconCont .logoW", .5, {css:{opacity: 1}, ease: Ease.none, delay: delay});


		TweenMax.to("#mainMenu", 1.5, {width:'100vw', ease: Power4.easeInOut, delay: delay});


		TweenMax.to("#menuSec1 .menuSecCont", .9, {y:0, ease: Power1.easeOut, delay: delay+=.6});
		TweenMax.to("#menuSec2 .menuSecCont", .9, {y:0, ease: Power1.easeOut, delay: delay+=.1});
		TweenMax.to("#menuSec3 .menuSecCont", .9, {y:0, ease: Power1.easeOut, delay: delay+=.1});
		TweenMax.to("#menuSec4 .menuSecCont", .9, {y:0, ease: Power1.easeOut, delay: delay+=.1, onComplete:function(){
			$('#projectWrap .project').css('transform', 'translateX(0)');
			$("#secVision .indexText, #secVision .secContent, .project .mainInfo").css('opacity', '0');
			$('#homeWrap, #projectWrap, #projectWrap #' + project).css('display', 'none');
			$('#homeWrap').css('opacity', '0');
			$('.tec, .ejec').removeClass('selected');
			$('.tec .underline, .ejec .underline').removeAttr("style");
			$('.project .images').css({'padding-left': '50vw', 'opacity': '0'});
			$('.project .content').css({'margin-left': '3vw', 'opacity': '0'});

			onAnim = false;

		}});


		$('#experiencesCont .experience').removeClass("active");
		TweenMax.to($('#descriptionsCont .description'), 0.2,{opacity: 0, marginLeft: '-2vw', ease:Power0.linear, delay: delay+=.5});
		TweenMax.to($('.crossActive'), 0.2,{opacity: 0, ease:Power0.linear, delay: delay});
		TweenMax.to($('.crossStatic'), 0.2,{opacity: 1, ease:Power0.linear, delay: delay});
		TweenMax.to($('.crossActive'), 0.2,{opacity: 0, ease:Power0.linear, delay: delay});

		menuOpen = true;
		contactOpen = false;
		aboutOpen = false;
		projectOpen = false;

	}
}

function closeMenu() {

	if(!onAnim){

		onAnim = true;

		var delay = 0;

		TweenMax.to("#mainMenu", 1.5, {width:'4vw', ease: Power4.easeInOut, delay:delay});
		TweenMax.to("#menuHam .line", .5, {rotation: 0, ease: Ease.none, delay: delay});
		// TweenMax.to("#menuHam > .line1", .5, {top: '.5vh', ease: Ease.none, delay: delay});
		// TweenMax.to("#menuHam > .line2", .5, {top: '1.5vh', ease: Ease.none, delay: delay});


		TweenMax.to("#menuSec1 .menuSecCont", .6, {y: percentToPixel($("#menuSec1 .menuSecCont"), 100), ease: Power1.easeIn, delay: delay});
		TweenMax.to("#menuSec2 .menuSecCont", .6, {y: percentToPixel($("#menuSec2 .menuSecCont"), 100), ease: Power1.easeIn, delay: delay+=.1});
		TweenMax.to("#menuSec3 .menuSecCont", .6, {y: percentToPixel($("#menuSec3 .menuSecCont"), 100), ease: Power1.easeIn, delay: delay+=.1});
		TweenMax.to("#menuSec4 .menuSecCont", .6, {y: percentToPixel($("#menuSec4 .menuSecCont"), 100), ease: Power1.easeIn, delay: delay+=.1});

		TweenMax.to("#header #headerBg", .3, {opacity: 1, ease: Linear.easeNone, delay: delay+=.9, onComplete: function(){
			onAnim = false;
		}});

		menuOpen = false;

		///////////////// COLOR CHANGE /////////////////

		// TweenMax.to("#header", .5, {css:{backgroundColor: '#000000', borderRightColor:"hsla(0,0%,100%,.19)"}, ease:Ease.none, delay: delay+=.9});
		// TweenMax.to("#menuHamCont", .5, {css:{borderBottomColor:"hsla(0,0%,100%,.19)"}, ease:Ease.none, delay: delay});
		// TweenMax.to("#menuHam > .line", .5, {css:{backgroundColor: '#FFFFFF'}, ease: Ease.none, delay: delay});
		// TweenMax.to("#ems .em", .5, {css:{color: '#FFFFFF'}, ease: Ease.none, delay: delay});
		// TweenMax.to("#persInfo > div", .5, {css:{color: '#FFFFFF'}, ease: Ease.none, delay: delay});
		// TweenMax.to("#persInfo > div p", .5, {css:{color: '#FFFFFF'}, ease: Ease.none, delay: delay});
		// TweenMax.to("#separator > .line", .5, {css:{backgroundColor: 'hsla(0,0%,100%,.19)'}, ease: Ease.none, delay: delay});
		// TweenMax.to(".iconCont .logoW", .5, {css:{opacity: 0}, ease: Ease.none, delay: delay});
		// TweenMax.to(".iconCont .logoB", .5, {css:{opacity: 1}, ease: Ease.none, delay: delay});

		///////////////// COLOR CHANGE /////////////////
	}
}


// !MENU FUNCTIONS



function percentToPixel(_elem, _perc){
  return (_elem.parent().outerHeight()/100)* parseFloat(_perc);
}

function getOS() {
  var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

  if (macosPlatforms.indexOf(platform) !== -1) {
    os = 'Mac OS';
  } else if (iosPlatforms.indexOf(platform) !== -1) {
    os = 'iOS';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    os = 'Windows';
  } else if (/Android/.test(userAgent)) {
    os = 'Android';
  } else if (!os && /Linux/.test(platform)) {
    os = 'Linux';
  }

  return os;
}

