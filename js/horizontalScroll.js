function HorizontalSmoothScroll(target, ease){
  let content = document.querySelector(target);
  let children = content.children;
  let contentWidth = 0; 



      // console.log($(target).children('.images').children('.imgProjCont:nth-child(1)').outerWidth());

      console.log(window.getComputedStyle(children[0].children[0]).width);

      var aux = 0;

      for(let i = 0; i < children[1].children.length; i++){
        contentWidth += (parseInt(window.getComputedStyle(children[1].children[i]).width) + parseInt(window.getComputedStyle(children[1].children[i]).marginRight));
      }

      // console.log(aux);

      contentWidth -= 400;
  
  let requestId = null;
  
  let wrapper = content.parentElement;
  // wrapper.style.position = 'relative';
  // wrapper.style.overflow = 'hidden';

  

  let wrapperBounds = wrapper.getBoundingClientRect();

  // console.log(children[0].children[1].children[0]);
  // console.log(parseInt(window.getComputedStyle(children[0]).width));

  let scroller = {  
    // lower values will decrease how far it moves on scroll
    wheelMultiplier: getLineHeight(),
    // lower values will make the animation longer
    ease: ease,
    speed: 0,
    minX: 0,
    maxX: contentWidth ,
    x: 0
  };


  $('.tec').click(function(){

    scroller.maxX =  (parseInt(window.getComputedStyle(children[0].children[1].children[0]).width) + parseInt(window.getComputedStyle(children[0].children[1].children[0]).marginRight)) * children.length + parseInt(window.getComputedStyle(children[0]).width) ;

  });

  $('.ejec, #menuSectionsCont .menuSec').click(function(){

    scroller.maxX =  contentWidth; 

  });

  $('.logoAsterisco, #projects, #about, #contact').click(function(){
    setTimeout(function(){scroller.x = 0}, 2000);
  });


  $(window).resize(function() {

      content = document.querySelector(target);
      children = content.children;
      contentWidth = 0;

      
      for(let i = 0; i < children[1].children.length; i++){
        contentWidth += (parseInt(window.getComputedStyle(children[1].children[i]).width) + parseInt(window.getComputedStyle(children[1].children[i]).marginRight));
      }

      contentWidth -= 400;

      scroller.maxX = contentWidth;

  });
  
  wrapper.addEventListener("wheel", onWheel);
  
  function onFrame() {

    scroller.speed += -scroller.speed * scroller.ease;
    // scroller.y -= scroller.speed;
    if(getOS() == 'Mac OS')
      scroller.x -= Math.round(scroller.speed * 1000) / 10000;
    else
      scroller.x -= Math.round(scroller.speed * 1000) / 1000;

    if (scroller.x < scroller.minX) {
      scroller.x = scroller.minX;
      scroller.speed = 0;
    } else if (scroller.x > scroller.maxX) {
      scroller.x = scroller.maxX;
      scroller.speed = 0;
    }

    //var progress = scroller.x / scroller.maxX;

    content.style.transform = "translate3d(" + -scroller.x + "px, 0px,1px)";
    //progressBar.style.transform = "translate3d(0px,0px,0px) scaleX(" + progress + ")";

    requestId = null;
    // console.log(scroller.speed);

    if (scroller.speed) {
      requestId = requestAnimationFrame(onFrame);
    }
  }
  function onWheel(event) {
    if(projectOpen)
      if(event.deltaY < 0 ||event.deltaY > 0){

          let normalized;  
          let delta = event.wheelDelta;

          if (delta) {
            normalized = (delta % 120) == 0 ? delta / 120 : delta / 12;
          } else {
            delta = event.deltaY || event.detail || 0;
            normalized = -(delta % 3 ? delta * 10 : delta / 3);
          }

          scroller.speed += normalized * scroller.wheelMultiplier;

          if (scroller.speed && !requestId) {
            requestId = requestAnimationFrame(onFrame);
          }
      }
  }

  function getLineHeight() {
    let element = document.createElement("div");
    element.style["font-size"] = "128ex";
    document.body.appendChild(element);
    let value = getComputedStyle(element).getPropertyValue("font-size");
    let size = parseFloat(value, 10) / 128;
    document.body.removeChild(element);
    return size;
  }
}


var homeLeft = true;

function HomeHorizontalSmoothScroll(target, ease){
  let content = document.querySelector(target);
  let children = content.children;
  let contentWidth = 0;

  for(let i = 0; i < children.length; i++){
     contentWidth += parseInt(window.getComputedStyle(children[i]).width) + parseInt(window.getComputedStyle(children[i]).marginRight) ; 
  }
  
  let requestId = null;
  
  let wrapper = content.parentElement;
  // wrapper.style.position = 'relative';
  // wrapper.style.overflow = 'hidden';

  

  let wrapperBounds = wrapper.getBoundingClientRect();

  // console.log(parseInt(window.getComputedStyle(children[0]).width));
  // console.log(wrapperBounds.width);
  // console.log(parseInt(window.getComputedStyle(children[0]).width));

  let scroller = {  
    // lower values will decrease how far it moves on scroll
    wheelMultiplier: getLineHeight(),
    // lower values will make the animation longer
    ease: ease,
    speed: 0,
    minX: 0,
    maxX: contentWidth - wrapperBounds.width,
    x: 0
  };


  $(window).resize(function() {

      content = document.querySelector(target);
      children = content.children;
      contentWidth = 0;

      wrapperBounds = wrapper.getBoundingClientRect();

      for(let i = 0; i < children.length; i++){
         contentWidth += parseInt(window.getComputedStyle(children[i]).width) + parseInt(window.getComputedStyle(children[i]).marginRight) ; 
      }

      scroller.maxX = contentWidth - wrapperBounds.width;

  });
  
  wrapper.addEventListener("wheel", onWheel);

  $('#secDownload .goBack').click(function(){

      $('#homeWrap').addClass('scrollLeftHome');

      $('#homeWrap').css('transform', 'translateX(0)');
      scroller.x = scroller.minX;

      setTimeout(function(){$('#homeWrap').removeClass('scrollLeftHome');},800)
   
  });

  $('.logoAsterisco, #projects').click(function(){
    setTimeout(function(){
      $('#homeWrap').css('transform', 'translateX(0)');
      scroller.x = 0
    }, 2000);
  });


  $('#persInfo #contact').click(function(){
      if(aboutOpen){

          if(homeLeft == true){
            console.log('No hace easing');
            var delay = 0;
            $('#secPresentation, #secExperiencia, #secDownload').css('display', 'none');
          }
          else{

            console.log('hace easing');
            var delay = .7;

            $('#homeWrap').addClass('scrollLeftHome');

            $('#homeWrap').css('transform', 'translateX(0)');
            scroller.x = scroller.minX;

            setTimeout(function(){
              $('#homeWrap').removeClass('scrollLeftHome');

            },800);

            setTimeout(function(){
               $('#secPresentation, #secExperiencia, #secDownload').css('display', 'none');
            },1000);

        }

       

        TweenMax.to("#secVision .secContent.definition", .5, {opacity:0, ease: Linear.easeNone, delay: delay+=.3});
        TweenMax.to("#secVision .secContent.contact", .5, {opacity:1, ease: Linear.easeNone, delay: delay+=.3});


        setTimeout(function(){
          aboutOpen = false;
          contactOpen = true;

        }, 1000);
      }
  });
  
  function onFrame() {
    scroller.speed += -scroller.speed * scroller.ease;
    // scroller.y -= scroller.speed;

    if(getOS() == 'Mac OS')
      scroller.x -= Math.round(scroller.speed * 1000) / 10000;
    else
      scroller.x -= Math.round(scroller.speed * 1000) / 1000;

    if (scroller.x < scroller.minX) {
      scroller.x = scroller.minX;
      scroller.speed = 0;
    } else if (scroller.x > scroller.maxX) {
      scroller.x = scroller.maxX;
      scroller.speed = 0;
    }

    //var progress = scroller.x / scroller.maxX;

    content.style.transform = "translate3d(" + -scroller.x + "px, 0px,1px)";
    //progressBar.style.transform = "translate3d(0px,0px,0px) scaleX(" + progress + ")";

    requestId = null;
    // console.log(scroller.speed);

    if (scroller.speed) {
      requestId = requestAnimationFrame(onFrame);
    }
  }
  function onWheel(event) {

    if(scroller.x == 0){
      homeLeft = true;
    }
    else{
      homeLeft = false;
    }

    if(aboutOpen == true)
      if(event.deltaY < 0 ||event.deltaY > 0){

          let normalized;  
          let delta = event.wheelDelta;

          if (delta) {
            normalized = (delta % 120) == 0 ? delta / 120 : delta / 12;
          } else {
            delta = event.deltaY || event.detail || 0;
            normalized = -(delta % 3 ? delta * 10 : delta / 3);
          }

          scroller.speed += normalized * scroller.wheelMultiplier;

          if (scroller.speed && !requestId) {
            requestId = requestAnimationFrame(onFrame);
          }
      }
  }

  function getLineHeight() {
    let element = document.createElement("div");
    element.style["font-size"] = "128ex";
    document.body.appendChild(element);
    let value = getComputedStyle(element).getPropertyValue("font-size");
    let size = parseFloat(value, 10) / 128;
    document.body.removeChild(element);
    return size;
  }
}



// SCROLL DE LAS IMAGENES DE LOS PROYECTOS

// $('#projectWrap .project').each(function(){
//     new HorizontalSmoothScroll(this, 0.07);
// });

for(let i = 1 ; i <= $('#projectWrap .project').length ; i++)
   new HorizontalSmoothScroll('#projectWrap .project:nth-child(' + i + ')', 0.07);

// SCROLL DE LAS SECCIONES DE LOS PROYECTOS
// new HorizontalSmoothScroll('#projectWrap .project .content', 0.07);


new HomeHorizontalSmoothScroll('#homeWrap', 0.07);


